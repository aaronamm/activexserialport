﻿using System;
using System.Drawing.Printing;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Microsoft.JScript;
using Microsoft.Win32;
using Convert = System.Convert;
using System.Linq;
using System.IO.Ports;
namespace ActiveXMainProject
{

  
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface IControlEvent
    {
        //Add a DispIdAttribute to any members in the source interface to specify the COM DispId.
        [DispId(0x60020001)]
        void OnDataReceived(string dataReceived); //This method will be visible from JS

    }

    /// <summary>
    /// Event handler for events that will be visible from JavaScript
    /// </summary>
    public delegate void ControlEventHandler(string redirectUrl);


    [ClassInterface(ClassInterfaceType.AutoDual),
    ComSourceInterfaces(typeof(IControlEvent))] //Implementing interface that will be visible from JS
    [Guid("7A4F9CD3-DFE2-43D1-9B8C-A2F9925A1A81")]
    [ComVisible(true)]
    public class ActiveXSerialPortConnector
    {

        private string _portName;

        private SerialPort _serialPort;

        public ActiveXSerialPortConnector()
        {

        }

        public event ControlEventHandler OnDataReceived;
        /// <summary>
        /// Opens application. Called from JS
        /// </summary>
        [ComVisible(true)]
        public void Open(string portName, int baudRage)
        {
            //TODO: Replace the try catch in aspx with try catch below. The problem is that js OnClose does not register.
            try
            {

                _portName = portName;
                if (_serialPort == null)
                {
                    _serialPort = new SerialPort(_portName, baudRage);
                    _serialPort.DataReceived 
                        += new SerialDataReceivedEventHandler(_serialPort_DataReceived);
                }

                if (!_serialPort.IsOpen)
                {
                    _serialPort.Open();
                }
                MessageBox.Show(string.Format("connected to {0}", _portName));
            }
            catch (Exception e)
            {
                
                MessageBox.Show(e.Message);
            }
        }

        void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {

            var dataReceived = _serialPort.ReadExisting();
            //MessageBox.Show(dataReceived);
            if (OnDataReceived != null)
            {
                OnDataReceived(dataReceived);
            }
        }

        /// <summary>
        /// property visible from JS
        /// </summary>
        [ComVisible(true)]
        public string PortName
        {
            get
            {
                return _portName;
            }
            
        }


        public void Close()
        {
            if (_serialPort != null)
            {

                if (_serialPort.IsOpen) _serialPort.Close();
                _serialPort.Dispose();
            }

            MessageBox.Show(string.Format("disconnected from {0}",_portName));
        }

        public ArrayObject GetArray()
        {
            var objectArray = new object[] { "m1", "m2", "m3", "m4" };
            return GlobalObject.Array.ConstructArray(objectArray);
        }  
    }//end class
}